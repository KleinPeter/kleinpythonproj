##README##

#DESCRIPTION#
This program simulates aircraft take off time slots in an airport. The program will take in user input to select a text file which contains the information for each aircraft. The program will then create a take off schedule based on the text file information.

#TO RUN#
to run, make the python program "projTwo.py" runnable and run using Python command in the command prompt. Python 2.7.x is required.