import airlineInfo as aq
import time
import sys

"""
ProjTwo takes in a text document containing airplane takeoff information.
From this information it cerates a priority queue, while printing out the 
current status at every itteration of time.
"""

finalList = []		#list of final queue, with every airplane in order with actual start and end time
printQueue = []		#list to print for every time slot 
subQueue = []		#list sorted by submission time
reqQueue = []		#list sorted by request time

"""
subSort takes in a unsorted queue of airPlaneInfo objects
and sorts them by their submission time.
@param tempQueue: the list to be sorted.
@return the list sorted by submission time.
"""

def subSort(tempQueue):
	subQueue = []
	for y in xrange (len(tempQueue)):
		temp = tempQueue[y]
		#boolean if the tempQueue is empty
		add = True
		#put into a queue, sorted by submission time
		for x in xrange (len(subQueue)):
			#if it has a later submission in the queue already, 
			#put it in the correct spot and break the loop.
			#or else tell "add" to add the temp at the end.
			#if two have the same submission time, but the one
			#who requested first, first
			if temp.getSubTime() == subQueue[x].getSubTime():
				if temp.getReqTime() < subQueue[x].getReqTime():
					subQueue.insert(x, temp)
				else:
					subQueue.insert(x+1, temp)
					
				add = False
				break
			if temp.getSubTime() < subQueue[x].getSubTime():
				subQueue.insert(x, temp)
				add = False
				break
		if add:
			subQueue.append(temp)
	return subQueue
	
"""
reqSort takes in a unsorted queue of airPlaneInfo objects
and sorts them by their request time.
@param tempQueue: the list to be sorted.
@return the list sorted by request time.
"""	
		
def reqSort(tempQueue):
	reqQueue = []
	for y in xrange (len(tempQueue)):
		temp = tempQueue[y]
		#boolean if the tempQueue is empty
		add = True
		#put into a queue, sorted by request time
		for x in xrange (len(reqQueue)):
			#if it has a later request in the queue already, 
			#put it in the correct spot and break the loop.
			#or else tell "add" to add the temp at the end.
			#if two have the same requested time, but the one
			#who submitted first, first
			if temp.getReqTime() == reqQueue[x].getReqTime():
				if temp.getSubTime() < reqQueue[x].getSubTime():
					reqQueue.insert(x, temp)
				else:
					reqQueue.insert(x+1, temp)
					
				add = False
				break
			if temp.getReqTime() < reqQueue[x].getReqTime():
				reqQueue.insert(x, temp)
				add = False
				break
		if add:
			reqQueue.append(temp)
	return reqQueue
		
		
"""
calcTime calculate the actual start and end time for each airplane
in the list. The actual start/end can change every time a new airPlaneInfo
object is added
actual end = actual start + length.
@param sched: the list to calculate the time one
@return the new list with every objects actual start/end updated
"""
def calcTime(sched):
	#if the first entrys actual start has yet to be calculated,
	#its actual start is its time requested.
	if sched[0].getActualStart() == -1:
		sched[0].setActualStart(sched[0].getReqTime())
		sched[0].setActualEnd(sched[0].getActualStart()+sched[0].getLength())
	#calculate the actual start/end for every other spot in the list
	#if the spot before ends before our spots requested time, calculate accordingly
	#or else its start is its requested time.
	for x in xrange (1, len(sched)):
		offset = sched[x-1].getActualStart() + sched[x-1].getLength()
		if offset > sched[x].getReqTime():
			sched[x].setActualStart(offset)
		else:
			sched[x].setActualStart(sched[x].getReqTime())
		sched[x].setActualEnd(sched[x].getActualStart()+sched[x].getLength())
	return sched
		
"""		
checkRemoval checks every airplane to see if their actual endtime is greater than the time
if so, they lifted off so remove them from the list.	
@param list: list to check removal on.
@param time: current time
@return updated list	
"""

def checkRemoval(list, time):
	for x in list:
		if x.getActualEnd() <= time:
			#append to the final list in order to print out the final schedule
			finalList.append(x)
			list.remove(x)
	return list
		

		
#the with statement handles opening and closing the file
#promt the user for a text file. For every line in the 
#file create an airplane object and pass all of its information
input = raw_input("Plase input file name (ex: myFile.txt): ")
if input[-4:] != '.txt' and input[-4:] != '.csv':
	print "ERROR: Not a .txt or .csv file."
	sys.exit()


with open(input, 'r') as file:
	tempQueue = []
	for line in file:
		#split the info, store it into an airPlaneInfo object
		info = line.split(", ")
		#check if input is valid
		if info[2] < info[1]:
			print "ERROR: input invalid. Requested time is before submission time."
			sys.exit()
		try:
			int(info[1])
			int(info[2])
			int(info[3])
		except ValueError:
			print "ERROR: input invalid. Check input document."
			sys.exit()
		temp = aq.airPlaneInfo(info[0],info[1],info[2],info[3])
		tempQueue.append(temp)
	subQueue = subSort(tempQueue)
#if the input was empty
if len(subQueue) == 0:
	print "EMPTY INPUT"
	sys.exit()
	
#time loop, formatting the print queue from the original sub queue
#while the final list doesnt contain all the airplanes...
#first, check if anything in the print queue needs to be deleted
#second, if the time equals the submission time of any plane, 
#	add it to the print queue and calculate its start/end time
#third, print out the atatus of the print queue.
#lastly print the final ordered queue

clock = 0
endNum = len(subQueue)
while len(finalList) < endNum:
	printQueue = checkRemoval(printQueue, clock)
	for x in xrange(len(subQueue)):
		#if its time to add them to the queue, add them
		if subQueue[x].getSubTime() == clock:
			printQueue.append(subQueue[x])
		if subQueue[x].getSubTime() > clock:
			break
		printQueue = reqSort(printQueue)
		if len(printQueue) > 0 :
			printQueue = calcTime(printQueue)
	#delay to simulate time
	time.sleep(0.5)
	print "TIME: %d    " %(clock),
	for x in xrange(len(printQueue)):
		if printQueue[x].getActualStart() <= clock:
			print "%s (Started at %d)," %(printQueue[x].getID(),printQueue[x].getActualStart()),
		else:
			print "%s (scheduled for %d)," %(printQueue[x].getID(),printQueue[x].getActualStart()),
	print "\n"
	clock+=1
print "*FINAL SCHEDULE*"
#final list
for x in xrange(len(finalList)):
	print "%s (%d-%d),"%(finalList[x].getID(),finalList[x].getActualStart(),finalList[x].getActualEnd()),
	
	
			
		
	
		


		
		
		
		
		
		