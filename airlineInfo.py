"""
airPlaneInfo is an class that creates objects that contains the id, submission time, 
request time and length of liftoff of an airplane. This class is used by ProjTwo.py.
"""

class airPlaneInfo:
	
	id = ""				#id of the plane
	subTime = -1		#submission time of the plane
	reqTime = -1		#requested lif off time of the plane
	length = -1			#length of liftoff time of the plane
	actualStart = -1	#actual start of the plane
	actualEnd = -1		#actual end id of the plane
	
	"""
	constructor, takes in an airplanes id, submission time, 
	request time and length of liftoff of an airplane.
	"""
	
	def __init__(self, id, subTime, reqTime, length):
		self.subTime = subTime
		self.id = id
		self.reqTime = reqTime
		self.length = length
		
	"""
	toString for this object. returns an airplanes id, submission time, 
	request time and length of liftoff of an airplane.
	"""
	
	def __str__(self):
		return " " + self.id + "\t" + self.subTime + "\t" + self.reqTime + "\t" + self.length
		
	"""
	getter. returns id of airplane.
	@return id of airplane.
	"""	
	
	def getID(self):
		return self.id
		
	"""
	getter. returns submission time of airplane.
	@return submission time of airplane.
	"""	
		
	def getSubTime(self):
		return int(self.subTime)
		
	"""
	getter. returns request time of airplane.
	@return request time of airplane.
	"""	
	
	def getReqTime(self):
		return int(self.reqTime)
		
	"""
	getter. returns length of airplane.
	@return length of airplane.
	"""	
		
	def getLength(self):
		return int(self.length)
		
	"""
	getter. returns actual start of airplane.
	@return actual start of airplane.
	"""		
	
	def getActualStart(self):
		return int(self.actualStart)
		
	"""
	getter. returns actual end of airplane.
	@return actual end of airplane.
	"""		
	
	def getActualEnd(self):
		return int(self.actualEnd)	
		
	"""
	setter. sets actual start of airplane.
	@param actual start of airplane.
	"""	
	
	def setActualStart(self, num):
		self.actualStart = num
		
	"""
	setter. sets actual end of airplane.
	@param actual end of airplane.
	"""	
	
	def setActualEnd(self, num):
		self.actualEnd = num
	